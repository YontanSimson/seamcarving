import numpy as np
import matplotlib.pyplot as plt


class SeamCarving(object):

    def __init__(self, image, is_vertical=True):
        self.image = image
        self.is_vertical = is_vertical  # TODO: transpose if horizontal seam is requested
        self.seam = None

    def carve(self, debug=True):
        # Calculate gradient image
        dx, dy = np.gradient(self.image)
        E = np.abs(dx) + np.abs(dy)

        if debug:
            fig, axes = plt.subplots(1, 3)
            ax = axes[0]
            ax.imshow(dx)
            ax.set_title('dx')

            ax = axes[1]
            ax.imshow(dy)
            ax.set_title('dy')

            ax = axes[2]
            ax.imshow(E)
            ax.set_title('E')

        M = np.zeros(E.shape)
        # First row
        M[:, 0] = E[:, 0]

        # Calculate cost image with dynamic programing
        for col in np.arange(1, self.image.shape[1]):
            row = 0
            M[row, col] = E[row, col] + np.min([M[row, col - 1], M[row + 1, col - 1]])

            row = self.image.shape[0] - 1
            M[row, col] = E[row, col] + np.min([M[row - 1, col - 1], M[row, col - 1]])

            for row in np.arange(1, self.image.shape[0] - 1):
                M[row, col] = E[row, col] + np.min(M[row - 1:row + 2, col - 1])

        if debug:
            fig, axes = plt.subplots(1, 1)
            ax = axes
            ax.imshow(M)
            ax.set_title('M')

        # Backtrack
        rows, cols = M.shape
        seam = np.zeros((cols, 2))  # (x, y)

        min_idx = np.argmin(M[:, cols - 1])
        seam[cols - 1, :] = np.array([cols - 1, min_idx])

        # from right to left
        for col in range(cols - 2, -1, -1):
            if min_idx == 0:
                min_idx = np.argmin(M[0:2, col])
                seam[col, :] = np.array([col, min_idx])
            elif min_idx == rows - 1:
                min_idx = np.argmin(M[rows-2:rows, col])
                min_idx = min_idx + rows - 2
                seam[col, :] = np.array([col, min_idx])
            else:
                prev_min_idx = min_idx
                min_idx = np.argmin(M[min_idx - 1:min_idx + 2, col])
                min_idx = prev_min_idx + min_idx - 1
                seam[col, :] = np.array([col, min_idx])

        self.seam = seam

        if debug:
            ax.plot(self.seam[:, 0], self.seam[:, 1])
            ax.set_title('seam')

    def plot_results(self):
        fig, axes = plt.subplots(1, 1)
        ax = axes
        ax.imshow(self.image, cmap='gray')
        ax.plot(self.seam[:, 0], self.seam[:, 1])
        ax.set_title('seam')
