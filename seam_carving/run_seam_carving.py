from seam_carving import SeamCarving

import argparse
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input_image", help="Input image filename")
    args = parser.parse_args()

    print()
    try:
        img = Image.open(args.input_image)
    except IOError:
        pass

    seam_carver = SeamCarving(np.array(img))
    seam_carver.carve()
    seam_carver.plot_results()

    plt.savefig('./vertical_seam.png')
    plt.show()


if __name__ == "__main__":

    main()

