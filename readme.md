# Seam Carving

Find the optimal path of minimal energy of a gray scale image from left to right

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

```
git clone git@bitbucket.org:YontanSimson/seamcarving.git
cd seamcarving
```

### Prerequisites

What things you need to install the software and how to install them

```
Python 3.5
git
```

### Installing

Create a virtual environment for python 3.5

```
python3 -m venv venv
source venv/bin/active
```

And then 

```
pip install -r requirements.txt
```


## Running the script

An example:
```
python seam_carving/run_seam_carving.py -i ./data/mario.png
```

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

